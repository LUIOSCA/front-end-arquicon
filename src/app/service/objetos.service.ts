import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TiposDocumentos } from '../clases/TiposDocumentos';



@Injectable({
  providedIn: 'root'
})
export class ObjetosService {



  // tslint:disable-next-line: ban-types
  private urlApi: String;
  private datosSeision: any;

  // tslint:disable-next-line: variable-name
  constructor(private _http: HttpClient) {
    this.urlApi = 'http://localhost/api-rest-arquicon/index.php/';

  }

  // tslint:disable-next-line: typedef
  getInformacion(origen: string): Observable<any> {
    return this._http.get(this.urlApi + origen);
  }

  getInformacionUnParametro(metodo:string,origen: string): Observable<any> {
    return this._http.get(this.urlApi + origen +metodo);
  }

  //Objeto completo es el pobjeto con los valores de cada campo, método  es el metódo que se llama en la clase que recibe, claseObjeto es el objeto que se envía(MUnicipios, oficinas, etc)

  enviarObjetoPost(objeto, metodo: string): Observable<any> {
    let json = JSON.stringify(objeto);//Convierte el objeto en un string json
    //console.log(json)
    let params = json;
    let headers = new HttpHeaders({ 'Content-Type': "application/x-www-form-urlencoded" }); //Psar a formato encoding para enviar el objeto en la cabecera
    return this._http.post(this.urlApi + metodo, params, { headers: headers });
  }

  modificarRegistro(idtido: number, tiposdocumentos: TiposDocumentos) {
    let url: string;
    url = this.urlApi + '/' + idtido;
    this._http.put(url, tiposdocumentos)
      .subscribe(
        response => {
          alert('modificado' + response);
        },
        (error) => alert('error modificar'+error)
      );


  }

  eliminarRegistro1(idtido: number) {
    let url: string;
    url = this.urlApi + '/' + idtido;
    this._http.delete(url)
    .subscribe(
      response => {
        alert('eliminado' + response);
      },
      (error) => alert('error eliminar'+error)
    );


  }

}
