import { TiposDocumentoComponent } from './component/tipos-documento/tipos-documento.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';


const routes: Routes = [
  { path: 'tipos-documento', component: TiposDocumentoComponent },
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'idtido', component: TiposDocumentoComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
