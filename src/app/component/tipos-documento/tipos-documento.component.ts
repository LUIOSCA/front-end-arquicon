import { TiposDocumentos } from './../../clases/TiposDocumentos';
import { HttpHeaders } from '@angular/common/http';
import { element } from 'protractor';
import { ObjetosService } from './../../service/objetos.service';
import { AppComponent } from './../../app.component';
import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-tipos-documento',
  templateUrl: './tipos-documento.component.html',
  styleUrls: ['./tipos-documento.component.scss']
})
export class TiposDocumentoComponent implements OnInit {
  public tiposdocume: TiposDocumentos;
  public tiposdocumentos: any = [];
  public mensaje: string;
  public mensajeSuccess: Message[];
  public idtido: number;
  constructor(private _obejtosService: ObjetosService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.obtenerTiposDocumentos();
    this.tiposdocume = new TiposDocumentos();

  }

  obtenerTiposDocumentos() {
    this.mensaje = '';
    let status = '';
    this._obejtosService.getInformacion('obtenerTiposDocumento')
      .subscribe(
        response => {
          console.log(response);
          this.tiposdocumentos = response.tipoDocumento;
        },
        error => {
          this.mensaje = 'Error Inesperado';
          console.log(error.message);

        }

      );

  }

  realizarRegistroTiposDocumento() {
    this.mensaje = '';
    this._obejtosService.enviarObjetoPost(this.tiposdocume, "registrarTipoDocumentos").subscribe(
      response => {
        let status = response.status; //si respondio con status SUCCESS
        if (status === 'SUCCESS') {
          if (response.resultado == 'S') {
            alert('Registro Guardado Correctamente' + response.id);
            this.mostrarMensaje('Registro Guardado Correctamente');
            this.obtenerTiposDocumentos();
            this.addMessages('prueba de mensaje');

          } else {
            alert('Ocurrió un error inesperado, por favor contáctese con el administrador');
            this.mostrarMensaje(response.mensaje);
            console.log(response.mensaje);

          }
        } else {
          this.mostrarMensaje('Ocurrió un error inesperado, por favor contáctese con el administrador');
          alert('Ocurrió un error inesperado, por favor contáctese con el administrador');

        }
      },
      error => {
        this.mostrarMensaje('Ocurrió un error inesperado, por favor contáctese con el administrador');
        alert('Ocurrió un error inesperado');

        console.log(error.message);
      })
  }

  encontrarTiposDocumentos(idtido: number) {
    let tiposDocumentos: TiposDocumentos = this.tiposdocumentos.find(this.tiposdocumentos.idtido == idtido);
    alert('encontrada')
    return this.tiposdocumentos;
  }

  modificar(idtido: number, tiposdocumentos: TiposDocumentos) {
    alert('modificado');
    this._obejtosService.modificarRegistro(idtido, tiposdocumentos)
  }

  eliminarTiposDocumentos(idtido: string) {
    this.mensaje = '';
    let status = '';
    this._obejtosService.getInformacionUnParametro(idtido, "eliminarTiposDocumento/").subscribe(
      response => {
        let status = response.status;
        if (status === 'SUCCESS') {
          if (response.resultado === 'S') {
            this.tiposdocumentos = response.resultado;
            alert('Registro Eliminado correctamente');
            this.obtenerTiposDocumentos();
          } else {
            alert('El registro no fue eliminado');
          }
        }

      },
      error => {
        this.mensaje = 'Error Inesperado contacte al administrador';
        console.log(error.message);
      }
    );
  }


  mostrarMensaje(mensaje: string) {
    this.mensaje = mensaje;
    setTimeout(() => {
      this.mensaje = '';
    }, 5000);
  }

  addMessages(mensaje: string) {
    this.mensajeSuccess = [
      { severity: 'success', summary: 'Success', detail: 'Message Content' },
      { severity: 'info', summary: 'Info', detail: 'Message Content' },
      { severity: 'warn', summary: 'Warning', detail: 'Message Content' },
      { severity: 'error', summary: 'Error', detail: 'Message Content' }
    ];
  }


}
