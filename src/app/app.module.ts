import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenubarModule } from 'primeng/menubar';
import { TiposDocumentoComponent } from './component/tipos-documento/tipos-documento.component';
import { HeaderComponent } from './component/header/header.component';
import { HomeComponent } from './component/home/home.component';
import {TabViewModule} from 'primeng/tabview';
import { FormsModule } from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {InputTextModule} from 'primeng/inputtext';

import {PanelModule} from 'primeng/panel';

///mensajes 
import {MessagesModule} from 'primeng/messages';
import { MessageModule } from 'primeng/message';


@NgModule({
  declarations: [
    AppComponent,
    TiposDocumentoComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MessageModule,
    MenubarModule,
    ButtonModule,
    TabViewModule,
    FormsModule,
    TableModule,
    HttpClientModule,
    InputTextModule,
    PanelModule,
    MessagesModule
  ],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
